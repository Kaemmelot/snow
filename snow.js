/**
 * MIT License
 *
 * Copyright (c) 2017 Kaemmelot (Sebastian Hofmann)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

(function () {
    "use strict";

    if ("snow" in window) {
        console.warn("Snow effect seems to be running already. You should avoid multiple includes!");
        return;
    }

    // Browser compatibility
    if (!Date.now)
        Date.now = function() { return new Date().getTime(); };

    function addEvent(object, type, callback) {
        if (object === null || typeof(object) === 'undefined') return;
        if (object.addEventListener) {
            object.addEventListener(type, callback, false);
        } else if (object.attachEvent) {
            object.attachEvent("on" + type, callback);
        } else {
            object["on"+type] = callback;
        }
    }

    function removeEvent(object, type, callback) {
        if (object === null || typeof(object) === 'undefined') return;
        if (object.removeEventListener) {
            object.removeEventListener(type, callback, false);
        } else if (object.detachEvent) {
            object.detachEvent("on" + type, callback);
        } else {
            object["on"+type] = null;
        }
    }

    (function() {
        // Original taken from https://github.com/darius/requestAnimationFrame which is under MIT License:
        /**
         * The MIT License (MIT)
         *
         * Copyright (c) 2013 Darius Bacon
         *
         * Permission is hereby granted, free of charge, to any person obtaining a copy of
         * this software and associated documentation files (the "Software"), to deal in
         * the Software without restriction, including without limitation the rights to
         * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
         * the Software, and to permit persons to whom the Software is furnished to do so,
         * subject to the following conditions:
         *
         * The above copyright notice and this permission notice shall be included in all
         * copies or substantial portions of the Software.
         *
         * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
         * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
         * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
         * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
         * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
         * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
         */
        var vendors = ['webkit', 'moz'];
        for (var i = 0; i < vendors.length && !window.requestAnimationFrame; ++i) {
            var vp = vendors[i];
            window.requestAnimationFrame = window[vp+'RequestAnimationFrame'];
            window.cancelAnimationFrame = (window[vp+'CancelAnimationFrame'] || window[vp+'CancelRequestAnimationFrame']);
        }
        if (/iP(ad|hone|od).*OS 6/.test(window.navigator.userAgent) // iOS6 is buggy
            || !window.requestAnimationFrame || !window.cancelAnimationFrame) {
            var lastTime = 0;
            window.requestAnimationFrame = function(callback) {
                var now = Date.now();
                var nextTime = Math.max(lastTime + 16, now);
                return setTimeout(function() { callback(lastTime = nextTime); }, nextTime - now);
            };
            window.cancelAnimationFrame = clearTimeout;
        }
    }());

    // JQuery equivalent height/width functions
    function getDocumentHeight() {
        return Math.max(
            document.documentElement.clientHeight,
            document.body.scrollHeight,
            document.documentElement.scrollHeight,
            document.body.offsetHeight,
            document.documentElement.offsetHeight
        );
    }

    function getDocumentWidth() {
        return Math.max(
            document.documentElement.clientWidth,
            document.body.scrollWidth,
            document.documentElement.scrollWidth,
            document.body.offsetWidth,
            document.documentElement.offsetWidth
        );
    }

    var prefixedProperty;
    (function() {
        // modified version from velocityjs (http://velocityjs.org/) which uses MIT License
        /*! VelocityJS.org (1.4.1). (C) 2014 Julian Shapiro. MIT @license: en.wikipedia.org/wiki/MIT_License */
        var properties = {};
        var vendors = ["", "Webkit", "Moz", "ms", "O"];
        var prefixElement = document.createElement("div");
        prefixedProperty = function(property) {
            /* If this property has already been checked, return the cached value. */
            if (properties[property])
                return properties[property];

            for (var i = 0, vendorsLength = vendors.length; i < vendorsLength; i++) {
                var propertyPrefixed;

                if (i === 0) {
                    propertyPrefixed = property;
                } else {
                    /* Capitalize the first letter of the property to conform to JavaScript vendor prefix notation (e.g. webkitFilter). */
                    propertyPrefixed = vendors[i] + property.replace(/^\w/, function(match) {
                            return match.toUpperCase();
                        });
                }

                /* Check if the browser supports this property as prefixed. */
                if (typeof prefixElement.style[propertyPrefixed] === "string") {
                    /* Cache the match. */
                    properties[property] = propertyPrefixed;
                    return propertyPrefixed;
                }
            }
            return property;
        }
    })();

    var init = function() {
        var overlay = document.createElement("canvas");
        if ("pointerEvents" in overlay.style) // Check if this browser can click through the canvas
            overlay.style.pointerEvents = "none";
        else {
            console.warn("This browser is not compatible with the snow effect. No snow for you :-(");
            return;
        }

        // global variables
        var running = false; // if the animation is running
        var snowControlHidden = typeof window.snowControlHidden === "boolean" && window.snowControlHidden; // to completely remove the control box set window.snowControlHidden to true before this runs
        var snowflakeImage = new Image();
        var viewHeight; // should be page size
        var viewWidth;
        var snowflakes = []; // all current snowflakes
        var translateX = 0; // current drawing position
        var translateY = 0;
        var frames = 0; // frame counter
        var last = null; // last time a frame was drawn - detect inactive document
        var count = 0; // number of snowflakes
        // fix iframe delayed loading problem - since this script might be cross domain, we cannot access the resize events
        var startTime = new Date();
        var iframeFix = false;
        // visible elements for reuse
        var fpsOut;
        var onOff;
        var speedInput;
        var densityInput;
        var snowControlDent;
        var snowControl;

        // Reset settings to default and save this in local storage if dontSave is not true
        function resetSettings(dontSave) {
            if (!running || !settings.enabled)
                window.snow.toggleFunction();
            settings = {
                enabled: true,
                multiplier: 1,
                baseSpeed: 1,
                controlOpen: true,
                controlTop: 200
            };
            window.snow = {count: 100, fps: 0, multiplier: 1, baseSpeed: 1, enabled: true,
                toggleFunction: window.snow.toggleFunction, resetFunction: window.snow.resetFunction};
            if (!dontSave)
                localStorage.setItem("kaemmelotSnow", JSON.stringify(settings));
            resize(true);
        }

        // Load settings
        /**
         * window.snow is READONLY from outside. Containing:
         * count - the amount of snowflakes based on site size and multiplier
         * fps - the frames per second (updated every 200ms)
         * multiplier - count is based on the size of the site. This is the multiplier to get the count
         * baseSpeed - the multiplier for speed
         * enabled - whether the effect is enabled
         * toggleFunction - use this to enable/disable the snow from outside, pass true to force a stop
         * resetFunction - use this to reset the snow from outside
         */
        window.snow = {count: 100, fps: 0, multiplier: 1, baseSpeed: 1, enabled: true,
            toggleFunction: function(stop) {}, resetFunction: function() {}};
        var settings = null;
        try {
            settings = JSON.parse(localStorage.getItem("kaemmelotSnow"));
        }
        catch (e) {}
        if (settings === null || typeof settings !== "object") {
            console.info("Invalid/Missing snow properties. Using default.");
            resetSettings();
        } else { // load old settings
            if (typeof settings.enabled === "boolean")
                window.snow.enabled = settings.enabled;
            if (typeof settings.multiplier === "number")
                window.snow.multiplier = settings.multiplier;
            if (typeof settings.baseSpeed === "number")
                window.snow.baseSpeed = settings.baseSpeed;
            if (typeof settings.controlOpen !== "boolean")
                settings.controlOpen = true;
            if (typeof settings.controlTop !== "number")
                settings.controlTop = 200;
            settings = {enabled: window.snow.enabled, multiplier: window.snow.multiplier, baseSpeed: window.snow.baseSpeed,
                controlOpen: settings.controlOpen, controlTop: settings.controlTop};
        }

        if (snowControlHidden) {
            // Control box was disabled
            resetSettings(true);
            settings.controlOpen = false;
        }

        running = settings.enabled;

        // Prepare snow and overlay
        // Image: snow.png
        // Image taken from http://demo.web3canvas.com/html5-css3/beautiful-christmas-santa-snow-falling-greetings-with-html5-css3/
        snowflakeImage.src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAYCAMAAADJYP15AAACT1BMVEX4+//4+//4+//4+//4+//4+//4+//4+//4+//4+//4+//4+//4+//4+//4+//4+//4+//4+//4+//4+//4+//4+//5+//4+//4+//4+//4+//4+//5/P/4+//4+//5/P/4+//6/P/4+//4+//4+//4+//6/P/4+//4+//7/P/4+//6/P/4+//5/P/4+//7/P/7/f/5/P/6/P/7/f/6/P/7/f/4+//4+//4+//6/P/7/f/6/P/4+//4+//6/P/7/f/4+//6/P/7/f/6/P/7/f/8/f/5/P/9/v/6/P/6/P/8/f/8/f/7/f/6/P/6/P/5/P/8/f/9/v/5/P/8/f/9/v/6/P/8/f/7/f/6/P/8/f/9/v/6/P/7/P/7/f/8/f/7/f/9/v/7/f/7/f/9/v/8/f/9/v/7/f/9/v/9/v/8/f/8/f/9/v/8/f/8/f/9/v/8/f/8/f/9/v/+/v/9/v/9/v/9/v/+/v/9/v/+/v/9/v/9/v/9/v/9/v/9/v/9/v/+/v/9/v/9/v/9/v/+/v/+/v/9/v/+/v/+/v/+/v/+/v/+/v/+/v/+/v/+/v/+/v/+/v/+/v/+/v/+/v/+/v/+/v/+///+/v/+/v/+///+///+///+///+///+///+///+///+///+///+///+///+///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////R5MSJAAAAxHRSTlMAAQIDBAUGBwgJCgsMDQ4PEBESExQVFRYXGBkaHB0eHh8fICEiJCQlJycoKSorLS8xMjMzNTY3OTo8PD5AQUFBQkNES0xPUVFSU1RVVldYWVlaW1tbYWFiY2NmaWptbXByc3R0dXd5fH1/gIKDhIaIiouOkJKTlZeXmJmbnp+goKGjpaWrrKyusLKztLW2t7m+v8HDxMbHycnMzs/Q0dLV1tfZ2tzd3t/g4ePk5ebo6err7e7v8PHy8/T19vf4+fr7/P3+CE8kbgAAAb1JREFUeAFlkfdzEkEAhfcSjoIKObl4EcVABGPRGHuJvffeezH2GHshaixiERUViyCW80ARUTnLURDPg/eHueOMDOL32347s/vePFKGtXIG8j/cydsuplrWss4Y2oy6fy90rjVTXhbmrWiteIcx1NXvwKuPhafotnM9av78ZGu0jbzRPudhEcDzjesCi+r7CQbCdcePbEXY07ReLnUOdl6Ed494XyD8M0BEcPj8qaK2aqW7C0kg2pcYR59/C7zeEJFkVfyw/C6Q8k03E8Yy+wGAb19ByXwCEN0u0Ji9HyN6NKABqXcAwgcDyA6iutcm71j3YQWfl04MQvV63PsO9aHaPGJJU+PVHKQJLVdQfDTGPrPNSoPX3cG9BQdk4MnNLNRLC0/lk/2ptuyPQwF+gaIBaWQuC7SlfVkQ+N4ZUXNaWu6IAYm9Tj3hzuXxsxgZf3yXqO481nIGGnDdRvgQQh2Qhg6YkSht8Ti64PNDbCCm1tUDR72/NulCAlD8s3antjUvnkxb6thas8vRjryifsFZxxAry/4dg9GPO732xY/NJ+aaGKoqlujZLGGaxVhDquBvvRlGZTV6vsFUPvwGKtSrCstrQtEAAAAASUVORK5CYII=";
        if ("touchAction" in overlay.style)
            overlay.style.touchAction = "none";
        overlay.style.position = "absolute";
        overlay.style.display = "block";
        overlay.style.top = 0;
        overlay.style.left = 0;
        overlay.style.bottom = 0;
        overlay.style.right = 0;
        overlay.style.margin = 0;
        overlay.style.zIndex = 2147483000; // max (2147483647) - 647
        overlay.style[prefixedProperty("transformStyle")] = "flat";
        overlay.style[prefixedProperty("userSelect")] = "none";
        document.body.appendChild(overlay);
        var context = overlay.getContext("2d");
        if ("globalCompositeOperation" in context)
            context.globalCompositeOperation = "lighter";

        // Shuffle an array
        // http://stackoverflow.com/a/6274381/5516047
        function shuffle(a) {
            var j, x, i;
            for (i = a.length; i; i--) {
                j = Math.floor(Math.random() * i);
                x = a[i - 1];
                a[i - 1] = a[j];
                a[j] = x;
            }
            return a;
        }

        // Snowflake class
        var Snowflake = function () {
            this.radius = 1;
            this.alpha = 1;
            this.startPos = {x: -1, y: -1, angle: 0};
            this.pos = {x: -1, y: -1, angle: 0};
            this.steps = {x: 1, y: 1, angle: 0.5}; // movement steps
            this.movement = { // values to randomize movement for every snowflake
                deltaX: 1,
                deltaAngle: 0,
                movement1: 1,
                movement2: 1,
                movement3: 1,
                movement4: 1,
                offset: 0,
                deceleration: 1
            };
            this.startTime = new Date();
        };

        // recalculate the current position based on the timestamp
        Snowflake.prototype.calculateCurrentPosition = function (now) {
            /**
             * The movement should be unpredictable. A pure sin function would be predictable.
             * Therefore we use a sin^3 function for the x movement (left/right) and
             * the integral of a sin^2 + const function for the y movement (slower and faster down).
             * The angle is a sin^2 function added on the current angle which should be random enough.
             */
            // let's get some movement values by using the time difference and a random delta offset to start
            var movement = (now.valueOf() - this.startTime.valueOf()) / this.movement.deceleration;
            var diffX = (movement + this.movement.deltaX) / 7;
            var diffAngle = (movement + this.movement.deltaAngle) / 100;
            var apc = this.movement.movement1 + this.movement.movement4; // a + c
            var cma = this.movement.movement4 - this.movement.movement1; // c - a
            this.pos = {
                x: Math.sin(this.movement.movement1 * diffX + this.movement.offset) * Math.sin(this.movement.movement2 * diffX + this.movement.offset / 2) *
                Math.sin(this.movement.movement3 * diffX) * this.steps.x + this.startPos.x,
                y: // base function: (Math.sin(this.movement.movement1 * movement + this.movement.offset) * Math.sin(this.movement.movement4 * movement) + 1) * this.steps.y + this.movement.movement2 * this.steps.y * 10
                // alias function: (sin(ax + b) * sin(cx) + d) * e + e * f * g
                // but we need the integral (constant C = startPos.y!!) i.e.
                // -e * sin((a + c) * x + b) / (2 * (a + c)) + e * sin((c - a) * x - b) / (2 * (c - a)) + f * e * x + C
                -this.steps.y * Math.sin(apc * movement + this.movement.offset) / (2 * apc)
                + this.steps.y * Math.sin(cma * movement - this.movement.offset) / (2 * cma)
                + this.movement.movement2 * this.steps.y * movement * 10 + this.startPos.y,
                angle: Math.sin(this.movement.movement1 * diffAngle + this.movement.offset) * Math.sin(this.movement.movement2 * diffAngle + this.movement.offset / 2) *
                this.steps.angle + this.pos.angle // this is different! Add it to the current angle, not the start. Otherwise it would only spin back and forth
            };
        };

        // Restart the snowflake at a new position with new values
        Snowflake.prototype.restart = function (radius, alpha, deltaX, deltaAngle, posX, posY, angle, stepX, stepAngle) {
            this.radius = radius;
            this.alpha = alpha;
            this.startPos = {x: posX, y: posY, angle: angle};
            this.pos = {x: posX, y: posY, angle: angle};
            this.steps = {x: stepX, y: Math.round(10 * settings.baseSpeed), angle: stepAngle};
            this.movement = {
                deltaX: deltaX,
                deltaAngle: deltaAngle,
                // Random movement values for every snowflake:
                movement1: Math.random(), // 1 and 4 shouldn't be too high or it gets hoppy
                movement2: Math.random() * 2.5 + 3, // 3 - 5.5 fits well for y speed I guess
                movement3: Math.random() * 5,
                movement4: Math.random(),
                offset: Math.random() * 500, // and a random offset
                deceleration: 10000 / (settings.baseSpeed * settings.baseSpeed)
            };
            this.startTime = new Date();
            this.calculateCurrentPosition(this.startTime); // start the movement and
            this.startPos = { // reset the position back to the start
                x: posX + this.startPos.x - this.pos.x,
                y: posY + this.startPos.y - this.pos.y,
                angle: angle + this.startPos.angle - this.pos.angle
            };
            this.pos = {x: posX, y: posY, angle: angle};
        };

        // restart a snowflake by generating random values
        // can be a new position (snowflake, offset[, startY]) or a new speed only (snowflake, true)
        function restartSnowflake(snowflake, offsetOrSpeedChange, startY) {
            if (typeof offsetOrSpeedChange !== "boolean") { // offset and possibly startY
                var radius = Math.floor(Math.random() * 5 + 5); // radius: 5 - 9
                startY = typeof startY !== 'undefined' ? startY
                    : Math.floor(Math.random() * -(typeof offsetOrSpeedChange !== 'undefined' ? offsetOrSpeedChange : 100) - radius);
                snowflake.restart(radius, // radius
                    Math.min(1, Math.ceil(Math.random() * 1000) / 1000 + 0.3), // alpha: 0.300 - 1.000 (1 has a higher chance)
                    Math.random() * 50, // deltaX
                    Math.random(), // deltaAngle
                    Math.floor(Math.random() * (viewWidth + radius * 2) - radius), // posX
                    startY,
                    Math.random() * Math.PI * 2 - Math.PI, // angle
                    viewWidth / 3, // stepX
                    Math.PI * settings.baseSpeed / 200 // stepAngle
                );
            }
            else // speed change: same pos, but new movement
                snowflake.restart(snowflake.radius, snowflake.alpha, snowflake.movement.deltaX, snowflake.movement.deltaAngle,
                    snowflake.pos.x, snowflake.pos.y, snowflake.pos.angle, viewWidth / 3, Math.PI * settings.baseSpeed / 200);
        }

        // draw the snowflake
        function drawSnowflake(snowflake) {
            // move to position
            context.translate(snowflake.pos.x - translateX, snowflake.pos.y - translateY);
            translateX = snowflake.pos.x;
            translateY = snowflake.pos.y;
            // rotate and set alpha
            context.rotate(snowflake.pos.angle);
            context.globalAlpha = snowflake.alpha;
            // draw
            context.drawImage(snowflakeImage, 0, 0, snowflakeImage.width, snowflakeImage.height,
                -snowflake.radius, -snowflake.radius, snowflake.radius * 2, snowflake.radius * 2);
            // rotate back for further movement
            context.rotate(-snowflake.pos.angle);
        }

        // main drawing function
        function redraw() {
            if (!settings.enabled) { // Shut down
                running = false;
                fpsOut.innerHTML = window.snow.fps = 0;
                fpsOut.style.color = "black";
                context.clearRect(0, 0, viewWidth, viewHeight);
                snowflakes = [];
                frames = 0;
                last = null;
                return;
            }
            if (!running) // Start up
                window.snow.enabled = running = true;

            var now = new Date(); // calculate everything with one timestamp
            if (last === null)
                last = now;
            var i, c;
            var speedChange = window.snow.baseSpeed !== settings.baseSpeed; // detect speed change
            var newSnowflakes = [];
            var snowflake;
            var countOk = count >= snowflakes.length; // are there too many snowflakes?
            var remove;

            if (speedChange) {
                window.snow.baseSpeed = settings.baseSpeed;
                context.clearRect(0, 0, viewWidth, viewHeight); // A speed change may take some time, so clear the screen for now until redraw
            }

            // IFrame delayed loading fix - after 5 seconds
            if (!iframeFix && (now.valueOf() - startTime.valueOf() > 5000)) {
                iframeFix = true;
                if (getDocumentHeight() !== viewHeight || getDocumentWidth() !== viewWidth)
                    resize(true);
            }

            // New positions, restart and remove
            if (!countOk)
                remove = shuffle((Array.apply(null, {length: snowflakes.length})).map(Number.call, Number))
                    .slice(0, snowflakes.length - count); // Random snowflakes to be removed
            for (i = 0; i < snowflakes.length; i++) {
                snowflake = snowflakes[i];
                if (!countOk && remove.indexOf(i) >= 0)
                    continue; // Remove
                snowflake.calculateCurrentPosition(now);
                if (snowflake.pos.y - snowflake.radius >= viewHeight) { // outside view?
                    if (now.valueOf() - last.valueOf() > 500) // more than half a second inactive? -> inactive tab, restart inside the view
                        restartSnowflake(snowflake, 0, snowflake.pos.y % (viewHeight + 100)); // keep offset to avoid empty space
                    else
                        restartSnowflake(snowflake);
                }
                else if (speedChange)
                    restartSnowflake(snowflake, true);
                newSnowflakes.push(snowflake);
            }
            snowflakes = newSnowflakes;

            // Add new snowflakes
            c = snowflakes.length;
            while (count > c) {
                c++;
                snowflake = new Snowflake();
                restartSnowflake(snowflake, -viewHeight - 100); // add negative offset to spread and avoid empty space
                snowflakes.push(snowflake);
            }

            // Redraw
            context.save(); // don't restore before all snowflakes are drawn -> performance (therefore: use translateX und translateY)
            context.clearRect(0, 0, viewWidth, viewHeight);
            for (i = 0; i < snowflakes.length; i++)
                drawSnowflake(snowflakes[i]);
            context.restore();
            translateX = 0;
            translateY = 0;

            // FPS measurement
            frames++;
            now = new Date();
            if (now.valueOf() - last.valueOf() > 200) { // update fps all 200ms
                var lastFps = window.snow.fps; // zero here means stopped animation and black
                window.snow.fps = Math.round(frames / ((now.valueOf() - last.valueOf()) / 1000));
                if (settings.controlOpen && lastFps !== window.snow.fps) { // only if open and changed
                    fpsOut.innerHTML = window.snow.fps;
                    if (window.snow.fps >= 45 && (lastFps < 45 || lastFps === 0)) // change color only if needed
                        fpsOut.style.color = "green";
                    else if (window.snow.fps >= 30 && window.snow.fps < 45 && (lastFps >= 45 || lastFps < 30 || lastFps === 0))
                        fpsOut.style.color = "darkorange";
                    else if (window.snow.fps < 30 && (lastFps >= 30 || lastFps === 0))
                        fpsOut.style.color = "red";
                }
                frames = 0;
                last = now;
            }

            // loop
            window.requestAnimationFrame(redraw);
        }

        // recalculate snowflake count based on viewport and multiplier
        function recalcCount() {
            window.snow.count = count = Math.round(viewHeight * viewWidth * settings.multiplier / 7500);
        }

        // resizeEvent for the document. Pass true to recalculate the count afterwards
        function resize(calcCount) {
            overlay.height = 1; // temporary set to 1, so it doesn't effect the real page size
            overlay.width = 1;
            viewHeight = getDocumentHeight();
            viewWidth = getDocumentWidth();
            overlay.height = viewHeight;
            overlay.width = viewWidth;
            if (calcCount === true)
                recalcCount();
        }

        // open/close the control box (event handler)
        function toggleControl() {
            settings.controlOpen = !settings.controlOpen;
            localStorage.setItem("kaemmelotSnow", JSON.stringify(settings));
            snowControlDent.style.right = (settings.controlOpen ? 132 : -18) + "px";
            snowControl.style.right = (settings.controlOpen ? 1 : -152) + "px";
            if (!settings.controlOpen) { // disable fpsOut
                fpsOut.style.color = "black";
                fpsOut.innerHTML = 0;
            }
            else
                window.snow.fps = 0; // causing a refresh of fpsOut in redraw
        }

        var controlMoveStart = null;
        var controlMoveTop = settings.controlTop;
        // control moving event handler (mouse or touch)
        function onControlMoving(event) {
            event = event || window.event;
            event.stopPropagation();
            event.preventDefault();
            if (!("touches" in event) && (event.buttons & 1) !== 1)
                moveControl(false, event); // stop if mouse not pressed anymore
            else { // set new top, but don't save it yet
                controlMoveTop = settings.controlTop + ("touches" in event ? event.touches[0] : event).screenY - controlMoveStart;
                snowControlDent.style.top = controlMoveTop + 20 + "px";
                snowControl.style.top = controlMoveTop + "px";
            }
        }

        // start/stop moving of the control box (event handler)
        function moveControl(start, event) {
            event = event || window.event;
            event.stopPropagation();
            event.preventDefault();
            if (start && controlMoveStart === null) {
                controlMoveStart = ("touches" in event ? event.touches[0] : event).screenY;
                addEvent(document, "mousemove", onControlMoving);
                addEvent(document, "touchmove", onControlMoving);
            }
            else if (!start && controlMoveStart !== null) {
                removeEvent(document, "mousemove", onControlMoving);
                removeEvent(document, "touchmove", onControlMoving);
                var e = ("touches" in event ? event.touches[0] : event);
                if ((typeof e === "object") && ("screenY" in e)) // touchcancel might not have this, use last value instead
                    settings.controlTop += e.screenY - controlMoveStart; // Save new position
                else
                    settings.controlTop = controlMoveTop;
                snowControlDent.style.top = settings.controlTop + 20 + "px";
                snowControl.style.top = settings.controlTop + "px";
                controlMoveStart = null;
                localStorage.setItem("kaemmelotSnow", JSON.stringify(settings));
            }
        }

        addEvent(window, "resize", resize);
        var start = function () {
            resize(true); // trigger first resize
            window.snow.toggleFunction = function(stop) {
                window.snow.enabled = settings.enabled = !stop && !settings.enabled;
                localStorage.setItem("kaemmelotSnow", JSON.stringify(settings));
                if (!running && settings.enabled) // if it was stopped before: startup
                    window.requestAnimationFrame(redraw);
                onOff.style[prefixedProperty("box-shadow")] = settings.enabled ? "0 0 5px 5px rgba(0,175,0,0.5)" : "0 0 5px 5px rgba(225,0,0,0.5)";
                onOff.style.backgroundColor = settings.enabled ? "rgb(85,111,17)" : "transparent";
            };
            window.snow.resetFunction = function () {
                // Restart with clean settings
                resetSettings();
                snowflakes = [];
                speedInput.value = Math.round(settings.baseSpeed * 100);
                densityInput.value = Math.round(settings.multiplier * 10);
                snowControlDent.style.top = settings.controlTop + 20 + "px";
                snowControl.style.top = settings.controlTop + "px";
                controlMoveStart = null;
            };
            running &= snowflakeImage.naturalWidth !== 0; // Check for image loading problem
            if (running) // Startup animation
                window.requestAnimationFrame(redraw);
            else if (snowflakeImage.naturalWidth === 0)
                console.error("Could not load snowflake :-(");
        };
        if (snowflakeImage.complete)
            start();
        else
            addEvent(snowflakeImage, "load", start);

        /**
         * Setup control with css.
         * Idea from http://cssdeck.com/labs/christmas-button
         * controlSnow.png and controlDentSnow.png are selfmade.
         * snow.png is from demo.web3canvas.com (see above).
         * All other images are taken from Tribal Wars 2 (Innogames) with few modifications or combinations (addSnow.png).
         * @TODO: This is completly static and could be done better. For the pictures this is currently an advantage: No delayed loading and no problems with URLs.
         */
        if (snowControlHidden)
            return; // ignore all control stuff
        var tmpElem;
        snowControlDent = document.createElement("div"); // Dent containing Opener and Mover
        snowControlDent.id = "snowControlDent";
        snowControlDent.style.width = "42px";
        snowControlDent.style.height = "52px";
        snowControlDent.style.position = "fixed";
        snowControlDent.style.top = settings.controlTop + 20 + "px";
        snowControlDent.style.right = (settings.controlOpen ? 132 : -18) + "px";
        snowControlDent.style[prefixedProperty("box-sizing")] = "border-box";
        snowControlDent.style.zIndex = 2147483001;
        snowControlDent.style.background = "#a23227";
        snowControlDent.style[prefixedProperty("borderRadius")] = "10px";
        snowControlDent.style[prefixedProperty("box-shadow")] = "inset 1px 1px 0 rgba(255,255,255,0.25),inset 0 0 6px #a23227,inset 0 10px 26px -13px #ac3223,1px 1px 3px rgba(0,0,0,0.75)";
        snowControlDent.style.overflow = "visible";
        tmpElem = document.createElement("div"); // Opener
        tmpElem.style.display = "block";
        tmpElem.style.position = "absolute";
        tmpElem.style.top = "5px";
        tmpElem.style.left = "2px";
        tmpElem.style.width = "20px";
        tmpElem.style.height = "20px";
        // Image: openClose.png
        tmpElem.style.background = "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAaCAMAAAB1owf/AAAAb1BMVEUAAAASCgcKBgEPCwcJBwMLBwQJBQMIBAILBwQJBgMIBQIJBgMJBgMJBgMIBgQLCAUlGxJZPh2AVSOIaDWYbSefdzaqgj+3iCjBlEPLkR/MoVbSoDPZs2HbrUPk0afnvk/sx2Pv37Py1Xj311f///8RBTBRAAAAEnRSTlMAAwgPFh8nMTtIVWJvfZyvsLKBdkHhAAAA20lEQVR42rXR3XKDIBAFYH60RC2JNGm0iLCY93/HLstOmEynlzk3nPGbBQfEWyKlfK4vUdooSeugS2nQTW7UWPTopp6oQbIdts5m9/mB1OBI1vR9b2wEJgZ4eFuT98NZJIaUYA8x5pxjxE5UAUoShouzRglNEwcGPwGXDafECSEdTUpoSpzmJSQgoN0IwjIPuNv843n7nQ9CmLryB/OyBwjr/b5iPLYCSlTaQrA1ARaGSrfN8x3AersQMF2+V743f63Q6MryVaHReB5UKcN5JPjzclIZrf5/7XfkF905GQbaFoOuAAAAAElFTkSuQmCC) center center no-repeat";
        tmpElem.style.backgroundSize = "cover";
        tmpElem.style.cursor = "pointer";
        addEvent(tmpElem, "click", toggleControl);
        snowControlDent.appendChild(tmpElem);
        tmpElem = tmpElem.cloneNode(true); // Mover
        tmpElem.style.top = "26px";
        // Image: move.png
        tmpElem.style.background = "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAMAAACelLz8AAAARVBMVEUAAAAJBQNZPh3UsFQ/LBZgMhUvHw9FNB0gGREkHxNELBVNMRmYbSefdzaqgj+xjVC3iCjnvk/sx2Py1Xj311f75XL///+dvcVOAAAACXRSTlMAJ7Kzyt/i5uqUbM3LAAAAVUlEQVQoz92NoQ6AMBQDr2+TwP9/5wiGMIp4ZppMEGra5sTBP6OswlYQ3Bi3PqIV5TaYfURVRHX+09cMF4Vb6VIbURARyAT0PsG1QKULA+bwe9f38wBFDicKFP+VvQAAAABJRU5ErkJggg==) center center no-repeat";
        tmpElem.style.backgroundSize = "cover";
        tmpElem.style.cursor = "row-resize";
        addEvent(tmpElem, "mousedown", moveControl.bind(tmpElem, true));
        addEvent(tmpElem, "touchstart", moveControl.bind(tmpElem, true));
        addEvent(tmpElem, "mouseup", moveControl.bind(tmpElem, false));
        addEvent(tmpElem, "touchend", moveControl.bind(tmpElem, false));
        addEvent(tmpElem, "touchcancel", moveControl.bind(tmpElem, false));
        snowControlDent.appendChild(tmpElem);
        snowControl = document.createElement("div"); // The control div with all other elements
        snowControl.id = "snowControl";
        snowControl.style.width = "150px";
        snowControl.style.height = "170px";
        snowControl.style.position = "fixed";
        snowControl.style.top = settings.controlTop + "px";
        snowControl.style.right = (settings.controlOpen ? 1 : -152) + "px";
        snowControl.style[prefixedProperty("box-sizing")] = "border-box";
        snowControl.style.zIndex = 2147483002;
        snowControl.style.background = "#5e0d0c";
        snowControl.style[prefixedProperty("borderRadius")] = "15px";
        snowControl.style[prefixedProperty("box-shadow")] = "inset 1px 1px 0 rgba(255,255,255,0.25),inset 0 0 6px #a23227,inset 0 125px 100px -50px #ac3223,1px 1px 3px rgba(0,0,0,0.75)";
        snowControl.style.font = "monospace 14px";
        snowControl.style.overflow = "visible";
        speedInput = document.createElement("input"); // SpeedInput
        speedInput.title = "Speed";
        speedInput.type = "range";
        speedInput.min = 75;
        speedInput.max = 270;
        speedInput.step = 15;
        speedInput.value = Math.round(settings.baseSpeed * 100);
        speedInput.style.display = "block";
        speedInput.style.position = "absolute";
        speedInput.style.left = "36px";
        speedInput.style.top = "55px";
        speedInput.style.width = "100px";
        speedInput.style.height = "21px";
        speedInput.style.margin = "0";
        speedInput.style[prefixedProperty("box-sizing")] = "border-box";
        addEvent(speedInput, "change", function () {
            settings.baseSpeed = this.value / 100; // window.snow.baseSpeed will be changed once the difference is noticed
            localStorage.setItem("kaemmelotSnow", JSON.stringify(settings));
        });
        snowControl.appendChild(speedInput);
        densityInput = speedInput.cloneNode(true); // DensityInput
        densityInput.title = "Density";
        densityInput.min = 2;
        densityInput.max = 352;
        densityInput.step = 10;
        densityInput.value = Math.round(settings.multiplier * 10);
        densityInput.style.top = "83px";
        addEvent(densityInput, "change", function () {
            window.snow.multiplier = settings.multiplier = this.value / 10;
            recalcCount();
            localStorage.setItem("kaemmelotSnow", JSON.stringify(settings));
        });
        snowControl.appendChild(densityInput);
        onOff = document.createElement("div"); // OnOff
        onOff.title = "On/Off";
        onOff.style.display = "block";
        onOff.style.width = "26px";
        onOff.style.height = "26px";
        onOff.style.position = "absolute";
        onOff.style.left = "40px";
        onOff.style.top = "17px";
        onOff.style[prefixedProperty("borderRadius")] = "5px";
        onOff.style[prefixedProperty("box-sizing")] = "border-box";
        onOff.style[prefixedProperty("box-shadow")] = settings.enabled ? "0 0 5px 5px rgba(0,175,0,0.5)" : "0 0 5px 5px rgba(225,0,0,0.5)";
        onOff.style.border = "1px rgba(0,0,0,0.5) solid";
        // Image: OnOff.png
        onOff.style.background = "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAMAAACelLz8AAAAtFBMVEUAAAASCgcKBgEPCwcJBwMLBwQJBQMIBAILBwQJBgMIBQIJBgMJBgMJBgMGBAMnHRIIBgQLCAUlGxI/LBYjGRB3aT+ofDmVhGJgMhVuUiuLYDZFNB2ll2pNMRllPhhrUihzRCJ4UBF9XDSAVSOIaDWRYBmYbSefdzagh1Wqgj+3iCi4pXrBlEPMoVbNvXLSoDPUu4vbrUPhyobmwnHnvk/sx2Py1Xjy3JP311f75XL88Jb///8zsRImAAAAHXRSTlMAAwgPFh8nMTtIVWJvfYyYnK+wyszNz9jf4OPm/r5HjmAAAAEqSURBVCjPrZJLT8MwEIRnH27SphRxBQkkOPD/fxN3XklK4nh3OSQNLVfwydYnjXdnBvjnQ2dXZrjH+pYVEKckhKBfiFhFdV8XMF8ikpSS6DP1tCHGLKoL0V39CUykjbcEixWpPtn7LFPX0YUHAAZAIvfeERAKeLppRGhFuu/5WKKzKB9ld1ChZQzRJuWxN5RxMC/X3Jn7/BfzXYcvK9HCDLnw1ciLIKgOFLcp58m8TJpoHR4BBcINIEAWPxgAIjN0toFZk01xQu4vCRtRIRIV2eTWfRF0H1M+TGAHS/WIY3E/eUg0HHzLYK12D/raDsVPefEm3YoESFF5+9ZP2c9C6b2ilALDZ9+V4j8pk6jotoGb9ZMVi7MCEIkIAxFmFnHZDSKeF4n4U9m+ARR2jrhkkaYsAAAAAElFTkSuQmCC) center center no-repeat";
        onOff.style.backgroundSize = "cover";
        onOff.style.backgroundColor = settings.enabled ? "rgb(85,111,17)" : "transparent";
        onOff.style.cursor = "pointer";
        addEvent(onOff, "click", function () { window.snow.toggleFunction(); });
        snowControl.appendChild(onOff);
        tmpElem = onOff.cloneNode(true); // Reset
        tmpElem.title = "Reset";
        tmpElem.style.left = "84px";
        // Image: reload.png
        tmpElem.style.background = "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAGKklEQVR42qVVeWxURRjfNzPvmPf27Xa3u9vdZbctvRZaKEiXHksprd2WuxBawpWASkVukEMECVgD5Y4HtJwpFBAQhVJAWhBBjlgwogYQ/gADyB0iQURN5Pj82tAYslmoOslk5r35Zn7zfd/v+42hJc2qyk2jJgtMk5lHlegUsyaVKiL1qxLRLfjjGdt/10I3mo2KjEC5DotSObxHzH2DQFcoIsnmIjHZzRr7D2ChxhGarOgKc1EijMv0Wf88VhGA/I6W3wmhtVyiPdEpRyubKcSzFgNYTZxzkbbGg/IoIVMMhP2YHqvD8Y/aQ92SDJhdkvAEzRpwfRB651JlQcEzaItA7DiiB9yoiO6m28psmcalc2lxlsdziuPg49kZsH+RD/YtSIJjlbmwcLQfiEFoQNthiBETwSX2XKBmEItRUTSZJuPnpDZu49dDCr1wam0WXK3JgobVQThakQ/7FydDzbx02FaWB3vLk2FmccIjt43Xi4TkqpLAwwLgYnMuOIYhBadLi7K9t2rK0uBabQ5c3JEHZ6rT4PvqfDi5Jgv2L/HDjnldYMucbKiakQnrpvmhazvrKdwXbAYK2xRmYI3hwunsXpmeX7+tyoObB/vC1frecGFHPlzalQ9Xdgbgm7Uvw9Y5WbBxVgDWTk6B6untoIPP8VBkbCmXiM+kMDF8bpoTL9FePq/l9AEMz+2v+sHNQ/3gxsEiOLc1COsmdYSMWBMMDkTDp2UBWDUhEVZMTocIo/pEkegCjEQnDLlulGl4kFVj+hmQXbFYJ0sGF8TC7fpCuHagL9w6VAQ7yzuDP8Z2WxbZbsboiZxkG2x8uxOUDY4Hq1G5iYU7H0E6IIBJFF5AApMmy3ibALLr1OFlGXCtvhfcayiBjbNfAm4g59BkssSETJGS11u7zOen9o8Dq66eJ4RMNcosUVWorFCD0JzvsA09MeEwNic56vGdgz3h1uF+cGp9Dnht2nWRkTEoNR5NohraJWEeX0uIMm3G44bhd7SmsKawvBAkQjMIChVcOJ1fPsoP1/Z1h99OlsCIfA8oIlvJGWmrc4E+PUjhMm0liyQBARzIUinFYmmZGjTGVcNCw+m6XeU5cGVvIdz5sg/0DHj+wGIdqSpML3THNtlGagqJMnLi1FViUxUSyeUQgOcCYSKjDQZhW92iVPj58x7w064iyEi2X0JZGeSIMCrNN/5fjQkG4alHG2sX5sPl3QVI527QtYP9LtJ9hFEVtTBALwIPtVFF4sZh7jvDU+HqHgTaEoQBATegR7NUkbpCN/0zd+uqoHMq2cwa1zjTuULNmEezLgs0SteeZYeuiCZBEIZ28VnhRl13OLO5AN4r7QhtoiP3MWLoYjOpciQPpZQT84WHOrEGO2EtlaColuI4FNmYiVGyc8lAnrmVJ8pMFVHIVrl8pGpGAI6syIO6xdnQt0siyFT4EBHS0Tsz1gtFclANO0ZBRcmJkUU60ufW9vQPuO8OCDjuF6baH0iMbTYqLN5qUp+ViUhVEDi+JzIjUwNtIu9vndke9iwMQOXYVCjOibvndVg+YZQMQdMM7KnYM7H3RgIt81r45YqJHVFsc+H0hm4wKugB/N+oFrEWXSahCq4QGW/t17m4fli26+GaicmwBkWzYnwKTC/2PerW1nk7zq6eiLWpR5Pc5u+6tvVcn9An+kH9B5lw4bMCOLulEBZhuB1mfg4vXIhEMsdH8FBWYE0QkypZFEaChLIv3gh6YfUEH6wclwjLS6OhcnwSrJvuh6q30p/UlOfCD5uCcLraDxd3IlNrg7BpVmeIcZiuIsBozJEL80PDUjBSlYnOpQiUnEJ0v3Zgmu3BopEJsHxUHCx/1QUrx8bD+ulpsH1uZ9g73we1ZUmwckoWvFmc9NiqSRdFRsc3Pv0agjRLUlgwp1kjKJRWZI3fYCDvKqJ4fGCW4+bovrF/je/ugGm9nTC6wANji+KfDAq0+qWN03gWt23BHPbHPHvxheZOTUWEFjSHRSNmLsnILi/GO4DevSJL8kK3hW9IiNJ3xNjU7ZrE3sf/UxtJgbSO11GqkO3EzuUXFXPoYqSJC/hiSkhVU1O9SDRBEWliU5doDBLHpstMMuPC8875GxKC0BgvWEYWAAAAAElFTkSuQmCC) center center no-repeat";
        tmpElem.style.backgroundSize = "cover";
        tmpElem.style.backgroundColor = "transparent";
        tmpElem.style[prefixedProperty("box-shadow")] = "0 0 1px 2px rgba(0,0,0,0.2)";
        addEvent(tmpElem, "click", function() { window.snow.resetFunction(); });
        snowControl.appendChild(tmpElem);
        tmpElem = document.createElement("label"); // SpeedIcon
        tmpElem.title = "Speed";
        tmpElem.style.display = "block";
        tmpElem.style.position = "absolute";
        tmpElem.style.width = "26px";
        tmpElem.style.height = "26px";
        tmpElem.style.left = "7px";
        tmpElem.style.top = "52px";
        // Image: time.png
        tmpElem.style.background = "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAMAAACelLz8AAAAzFBMVEUAAAASCgcJBwMLBwQJBQMIBAIIBQIJBgMJBgMGBAMIBgRZPh0/LBYjGRB3aT9VSDAvHw+LYDZFNB2ll2rIeD85JRFELBVHNSJNMRlUOR5aMRZdPiNiUDplPhhmRSlzRCJ2TzF9XDR9ZkWHXEeIaDWPblKQbUGThW2aekegdFugh1WomYGrcUivhmqxjVC0fle0t1u4m2C4pXq+k3jJom3KqIPMoVbNvXLOxKbUu4vXzrPZs2Hhxpnhyobk0afv37Pw58v8+Nn9873///+1XXgvAAAAFXRSTlMAAxYfJzFVYm+MnLLKzM3a4uPm/v7r0dctAAABTklEQVR42rWSbVuCMBiFIxJxoZYGMVcTX1i6YrgJ6mqB4///qDBYrx+7Ol/v65xnO89z9m+ybAfU6jm29YOcu2BAcynWQ+Da38gFGEiZkPlsHpMh6FhfCZMJ2+x2u4zHMww6n2kgFav962EscpFl/A4Ck2m5gz3ZcF0+POZ5kTEBPbeNtMEN3Zda66oqi0KueBIYmzNIGdf6+I7YRJAc9rsN6tFVVpsSeqxKhuZK8vgKNAiwxUul9fNkUaUY5UqxJDAojZbjg64EJPdYqK0ga9+gJ7wUoh4kEBVKCUEWxtW7nmSqrFWok0rJ0ahFziWiWf1s1ZCCJtBzzL+mkMrGU5RHxfFHHZY7DKdMFrVOhEX41IbpEPuI0TzeriVP8LQxmeZv/eCeLElEEMSoab6N7IC+7wdhGKIIee2+jGwXeKMwhCPPbPnXbYBufRt/0RsCqDU+uJdluwAAAABJRU5ErkJggg==) center center no-repeat";
        snowControl.appendChild(tmpElem);
        tmpElem = tmpElem.cloneNode(true); // DensityIcon
        tmpElem.title = "Density";
        tmpElem.style.left = "7px";
        tmpElem.style.top = "81px";
        // Image: addSnow.png
        tmpElem.style.background = "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAYCAMAAADJYP15AAAC91BMVEUAAAD4+//4+//4+//4+//4+//4+//4+//4+/8LBwP4+//4+/82MzD4+//4+//4+//4+//4+//4+//4+/8LCQX4+//4+//4+//4+//5+//4+//4+//4+//4+//4+//5/P/4+//4+//5/P9ubm34+//6/P/4+//4+//4+//4+//6/P/4+//BwsT4+//4+//6/P/4+//5/P/4+//7/P8IBAL7/f/5/P/6/P/7/f/6/P/7/f/4+//4+//4+//6/P/6/P/4+//4+//6/P/7/f/4+//6/P/6/P/7/f/5/P/9/v/6/P/8/f/7/f/6/P/6/P/5/P/8/f/9/v/5/P/8/f/9/v/6/P/8/f/7/f/6/P/8/f/9/v/7/f/7/f/9/v/U1tj7/f8cFg4pJRz8/f8ZEQj9/v/9/v/9/v/8/f/8/f/Z29z9/v/8/f/8/f/9/v/8/f/8/f/+/v8vIxE3Kxj9/v/+/v/9/v/9/v/9/v/9/v/9/v/9/v/+/v/9/v+0s7P9/v/+/v/+/v9KPSz9/v/+/v/+/v/+/v9LNiD+/v9DKxNlXVP+/v/+/v/+/v9RPyj+/v9GNBeNiHX+/v/+/v/+/v/+/v/+/v94dW/+///+/v/+/v/+//+Tj4X+///+//9dPSD+///+///+//+BfXf+//+8taD+///+//+Ae3P+//+alob+//+UjH3+//+bmI7+///////////////////////////////////////Kycn///////+ej3r///////99dGj////My8v////////////s7Oz////t7Oz////////29fX////29fX///////+Bc2D///////+Wk4+al5SamZTt7e329vb///////+gkHn///94al16aViCdWGUj4SWinaWkoafloWknZC/lDnKn0TNnz3StVrZsFDdumrfvF3gvGrhw2/kxnHmwVznv1bnzm3owFXpxF/rx2frzH3syWnty3LtzHPt1HHuzXTx2nv01Fz22Yf29vb////+4Xd5AAAA2nRSTlMAAQIDBAUGBwgJCQoLCwwNDg8QERISExQVFRYXGBkaHB0eHh8fHyAhIiQkJScnKCkqKy0vMTEyMzM1Njc5Ojw+QEFBQUJDS0xRUVJUVldYWVlaW1tbYWFiY2NmbXBydHR1dXV3d3x9f4CCgoOEhoiKjpKSkpWYmZuen6Cgo6WlpausrK6wsrOztLW1tre5ubq7vr/Bw8TFxsfJycvMzs/P0NHS0tXV1tfX2dna2tzc3d7f4OHj5OXm6Ojp6urr7u7v7/Dx8/P09PX29vf4+fr6+/z8/Pz8/P3+/mWKN3sAAAHSSURBVHgBY4ADVkFhdgZMIFzcrsqILsjMqrL+jz0HC6oEi2qIw+IzHgGmSOYwsgtJxP5Zuv3MzD/VssK8TGCbxJTEjJtT3Cdf//Pnz/zIsD5vCTkpdgbh6k250X/maqqF772Zp6tS9acsacVEKQbxWX/+rPgzxcDTccW1oECN8j/b/vxZJ8PAYV6x4c+fZREL1uw9v2Krf/efP3uanLkZGAXcJgFNPbj/DxAc3Qkk1sVIAZ0pOv3Puvy+a0BVG4FiczP6/hzXAgrzR5VZamQf/rPL13bKn/NlmhqpWZJAYW5DHzWl+hN/1tgY1f25PtVC1tVeEOhwoa4/PV7pe//8mdFy/Pel5Z3TTm6TBwoLpG36c/jPnwsgK688+PTyzrEaKaAvZf2m/PlzKG/B+RNXjxxY/eHJ7R2ZOmwMwqWn/py9vsC6IH7F5ZVrl3x8cePhvUViDOJz/szJ+bNGT9Fly/1f37++f/vmw+N90gxcpsHqZpsb7Cq3/Ln749W3zz+fPXnUCPQlCyszt6pCyp9Thy+eu7X7y7t5Hf3JjNAQZ7MqCV14Oq529vfXbSYinIiY4NNe88dJecLzp70iKPEm3rpKnyexqDCBE0WYTVyaC84BAHef1HZaaOtQAAAAAElFTkSuQmCC) center center no-repeat";
        snowControl.appendChild(tmpElem);
        tmpElem = document.createElement("label"); // FramesLabel
        tmpElem.title = "Frames per second (FPS)";
        tmpElem.style.display = "block";
        tmpElem.style.position = "absolute";
        tmpElem.style.left = "53px";
        tmpElem.style.top = "118px";
        tmpElem.style.width = "45px";
        tmpElem.style.height = "36px";
        tmpElem.style.zIndex = "0";
        // Image: frames.png
        tmpElem.style.background = "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC0AAAAkCAYAAAAdFbNSAAAIpElEQVR4Ae2Y6W9cVxnGoU5iO6kUWqRKFUKtQBWLUtLSgoAICh9KIlARVEqgLV9IoBJLA3xBtChCiEKpnLGT2I7Hy0yc8eJxvMSesevxePZ9Xzz2OPHECYSQD5Ha/gfn5X2u8qI7d+7ESRQhPmDp0Zm5M/f4d57znPecuR/5/999/D26u7UF2r3ro22ivXta90LtrS2PQXiN6/jeff0D3Cwd7mnd8QTUvrPlM0a17Xjo2e3E9x4Qte9qOSRq2/nQYYhfH4VwDd/H/7xnaMAC6Hbn6OzXrBNG7drRclLUtmvH0J301CcenRPtf+YLJejwoWc2v3Xo4A20r752ZLP37aOX8L8Afs/O4ibAvv6rn4dsnW8S5Oz/k7SaZh0W8s9bKL4yRIXYOG0U3CK6XgtC2usb5Qm0DcI9IvQhAjjM2hYawJh+cRbOffrxj+XMOgeoZ+wtU2EwY32/UEZ1/+UN0qvjD8dE2nv5HvqA64iMKaS4anT29797Q/3mpz9Ah4A0hQO4mfSOGd/rBw/3MRNoIRkwWsQG0KauYlHgQ3G2vXWnHx1sp38k+u9aaysdpjIOVmYILaAb4gFncfGFL3/uXeQHzoq7MvXNprXnr8e1dvidw3T+b0e0FpLvy3uZelHX8ZfR1n1fP3PSP67vadt5sQEaLmOFYsqMLmLKPrzmJtEH/0rR+/9M0Qc30qwUbVWW6NZWiG5dDdLNy166dS1M799Iap99eDMj94mkX+N6wADqwGVwAo0U1EEjv4iDfpFhKtHqVWRVc7Oa1jNTtJ4YoVp+hipRB1XiDiqG7XQ566TNzARdyfOAVy/Q9dIE3zcmoKYCPGIASIH+429fIVmUKI+ArittuIAPjB0L+N+5LcdGqZawUy48TNnQeW7tai0+QsWQnVKeXsqs9FF2xUql4BDlfP0q7e2lrL+PB2KjfNBGpWULsmu6ICXDiIy4feL1FwVcg4axxkV4AAUe02d0eSsxRPnIKBUiDsoF7ZRe6WeIc5QP2FQ+MKTQ5nwDKr3cpzJeq0q9101JHkRs4QyF57uUb6aDfHNdFFnsppDbQhVvB9qGhQdYgYaOv/JNDRpCChqgEXKsUEDXOw2HR6jM078esVHOP0gFBi+Gh1Upcl5VYmPcDlOZXxdD5/jzIXbYSqllq4oudGsKu06r4PwZFVnsVXFPH4UXu1XQfYoS0+9QcO6kAnDG+ecG6KPf/5q4rUFjN67bnlHmBFpg9eDIZC1ho9WYQxWCNlWJj6nL2SmqJp2qmpxQ6yxcW42NqnJ0RGFQWb+N4fspttijIgtnVJRhc8tdKuiyUGDeooxOI7uAlkoi0KhgTaGx6yDTUvSN8LXsBK0nx1SZnd1IObEA1WZumjXDA5hWG+lJtZYYZ+hRhnaojG8ITnM8TirfrIVC/No/3ymQcFcqB1pktw76hwefI0QEwpkG0MYaffTYT35EgDYDvpa0UyUxQdXEOK3FR6macqpLDForzKorhYsadDU1yY47Ac05t6v0yqAKu8+olZmT5L94ikKLvSTQAtsMGhJoON4UGtOgB9UvxGp2hqqZSVpLjhM7Spv5WXZ4Vm0V51QtD2g4fYE/n0BEqBAa5mwPUtjVTb7ZTixEyNRlgQYw4jDw1sE6aLTYQ7CXNGwsCLyUJLRGt4txbsMOXpjjaiMzxS7Pqa2SizWvMIhq6gLHw8nQ47woHZRcHqTIQi8F5k6pgOs0S4NWeqdF+J8AFmgMALB4/9I39jVCw3bYj4Ug0Ea3Ab+anKRscJihx9RaclJd5g2mVpinrZKbX19U1fQ0rgOa8qHzXCn6VcjdQwEXsnwK0cACpNTk23XAyK8RGgYCFu+//pWnCUlAIhqgcS6GA2YL8WphgkoxziovMDjNcFRNT9GVoouulhe4nefIAHyKStExSvvsXJf7KOjq4SyflSyLTKERBUAiGmgFev8Te6+bQqOkYEdqdnQsxTkeUd79IiO0GucqkrpAlzjTgL62uqAAjIVZ5VxXOCI5djqxPEDR9/ooyu2dgCFc10PjtR4a1Q1VruHcgU700JLrImuTazTynON4FCKjqqI5PU2XsjOQgjY4HuuoIJztJN8fXbJS1GMlbCTitNmPA4GWuiyLENCICX54YPPToOXcAWjs7QLd6LQsRCdlgnzmCDngJtyGuGI4eWFOq9X4uCpxucPnYXY4xg7HvAPEi/COLst7I7QsRJzpAY2du+6whKOffgqNVUSUDXNEoqNUjo8jJorjwhrVcpxn2EyAS11wmO8dZOBB8rsky81dlgHooeGylDw5lgK67tyBw1Kzn0hQkhcWDk1ZdpoXI2tEW5QcF01cLSjpHSLkOM6w+tObyAxYoAEq0KgeB7/0KQ0a1+SwBIMbDkvo2Oi23vUY6i5Pe4KhUj4bpVkZv11xq7KBcyrF30GliHist6EHm8bCCI26LNDQV596RHP7xec/eROVAzW6DhrW4/nCdj9Okc+gu4fBz1LMY1WxJSuDs7veARVmWNRkPl/wptJHBU/HXbmsPzsLNCKx9+HdCusMwHC5rnIAGiUPvw3lwI2OzQfQSeGls7wdd5Jn+l1A8omtm9vu21v0aYosWVWzWBihsZkZoaWV34RwGMD/cVkWov5xAesERoibUGq+++0DdOzVl2QwAoLay672kHfGQt5Ziwq4T1OYD0TYqrcDFul/zMLdX778WTrx4+frzhrga/okCaPBl+Q5nDxTk8df8ogL5Qc5w+HK3vUmqgMGwDnu5/hoEWoaCyO0OI3rT368nZ58/JFbL+x7LAPjEFkw3f+DRsNgZEYwEMzIs1/cXz7ynec2f/ba9/TxagpshIYe3t1aQJ/6h41gaE55j4ORgciTT+MDSRkM4oVZQUbl6ZRZPHAd96Cv+3+s+wBmBAASMf1gZL3se/rzN5FdDEae06GP/4kH5RiQ2WCMz5/x+QOJw39jdkR3Gwl8599syeypTAih/QAAAABJRU5ErkJggg==) center center no-repeat";
        snowControl.appendChild(tmpElem);
        fpsOut = document.createElement("div");
        fpsOut.title = "Frames per second (FPS)";
        fpsOut.innerHTML = 0;
        fpsOut.style.display = "block";
        fpsOut.style.position = "absolute";
        fpsOut.style.width = "26px";
        fpsOut.style.left = "60px";
        fpsOut.style.top = "130px";
        fpsOut.style.textAlign = "center";
        fpsOut.style.zIndex = "1";
        fpsOut.style.color = "black";
        snowControl.appendChild(fpsOut);
        tmpElem = document.createElement("div"); // snow on control
        tmpElem.style.display = "block";
        tmpElem.style.position = "absolute";
        tmpElem.style.width = "152px";
        tmpElem.style.height = "27px";
        tmpElem.style.left = "-1px";
        tmpElem.style.top = "-9px";
        tmpElem.style.pointerEvents = "none";
        if ("touchAction" in tmpElem.style)
            tmpElem.style.touchAction = "none";
        // Image: controlSnow.png
        tmpElem.style.background = "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAAAbCAYAAACeL3bkAAAPL0lEQVRo3u1bW4xd1Xn+/rX23ud+5uqxx2PAhtrUBuoATtqQUpXm1hQpVR5QRR9o1KoPERIvUaS0UlXRPlSNVNSkpQqpaCt6IRKBUkKiKDEBGUzAAV+xwePB+DY3z5yZc9/Xtf4+nLXnrDnMMDa4xYhZ0vKas/c+66z//7//vk24ygYzL/tMRPioDkNLSoAwkwFos/KVovFq4xt92Iy3GcDMAoBMPwJQRMTvk8F0ud+9wmCSAFwAOQAZAHmzAkAbQB1AACA0dF4STSkYU/qY2TGAlRZwY7OnvmqB1asJH0QbrL1SpgsAjpnSTDLMjg3jE4uZ7zWENQFAWVaBe2gV1jXuEZro2Vf3rDbvZM/z6bUMgAEAgwA2A+gDsAtA2cwGgNcAHARwwYAsSvlq8Snlj7B4o63PZQBFAAVzzTP8alrA9c2qLVqXWcuVZLwqYC5R9rQGEMgQRpYl0UbofKkWwRzcNQwfMgzfZBg+AuAzhilaKfV9KeUZALMAaua3yDAuZSwZBirrXgHAMIAN5pm2Yagwa/rcgFkz5kxk0ZW6qYsG3JH5ndjMxEwy3/UspcgYIW8GcBOAO81ZhpjZJaLre9milPomMz/nOM5ZA4TY3Ev3zps9S8byBebeBgDXAbjb3EuVomidMQEQKaUe1lq/4bpu21yTFrCUmY61h7bAl9JKFh8SAHot2dMKIPKsmZrvVHNSM1s3gksZn6Rm1+xjA8EzYLoewE4At5v5K5aFca3n4ziOH3Rd92UAMz1C9Cxrp4zGbgbwuwA+b8DYMCQVzPOhmXkDvLylJFjBOiVGyw8opfZLKecAVABUDc0tw4ccgI3mDDdorUeFEPcYQYu1dA2AX6vV/jmO4+dyudz5QqGwYAkyb8CzGcAWANsADGqtS0KI3zP0X+pItNZ7ARwRQvhm/8goLVl8TUEGI+Oq+ewCyAKYBzBpvlc1a7CauyUDhDQW6Acwysw7ieizBhB2zCMALAI4BmDcaPaiMb0twxhYZjmjtd5CRF8EcDcR3XKp3NBaP0NELSKy3UHqYq4FMNbzlcic0QEgoyiC4zhaCEGWpbMV6nL8+TQzTxKRn2qtsUJ3fYAwRMdxfC6KolOe5x11HOcUEUUABDOXAWwmoj3MfB0RbbsKcpFpACcAtJj5OSI6CuC0AVxgrNgyYHlGy65RSu0moj8QQvy2AdpqIzLa2zAgmzBWoWXFR0Na65uFEHdZbuf/OzHgDztBee8zLmlsQsZaEC3FfsJyUVfbiAA0tNYPCSFeBPAWgAUiUjawhpVS1yilfsN13a9djlW5agVmBV922M/mn5WiebICg2XROF05ybIBk05XArRlR4UABAMyPQddeb5c6dIAM5+M4/gbUspfSCkXU3A5SZKMRVH06Xw+/6CJQT7SiIoBRGbV3AGJTs0CAMXLn0+FlwIrTfME9ax24EiX5kvtrCAxZ4q5cy7FHfBrAzAwIHQHVB4ZE0+AQ+9DqbibEms7KreUyk47l2IDi66l+oWhfzViiWgHEX2t1WpVkiQ5YrwWHN/395RKpYevhFnnS6wLrCWIy9HUlJExA4EGAgZCbcDFXctkgiytGWCClqmRIAjmLgiZLMZSdyWzSnSFT1hd8GSkqQGE5mxxh0GaOxIlxaBYd0CX8i7dOyeAjPlb0tq81zaAubMm3L3GdurbIySR0mLokQS4wkT1BGREN31eQTbkuu6XKpXKk1rrtxcXF/2BgQHtzMzMlEul0mXb01QbtHX4hLs1AG0LlZa7F7uAZBdmlhWjaDmhKUEpgBV3wNPUQFMBDd0RYKy7dQPrN1gKgAiRBhbr87W3T752wK9XZvtGtmwt3XDLJzbny8U+7slSBHWLR9xbrDL3HNEFLtkFMeqcNdZA0gn5FAHKlYg9F40Dz7/UKAwOZUd37NwSAyLhrpClods1wBIrZBxsWSHdw3MbPMrimeilRQAkAOLlxTKPEDQrjXq5XBrJGIDnZfc8jmXN0/NIKXdUKpW+8fHxKoDQmZ6edrdv335JFinVipiXz8hoZMRApE3Zl5dXHm2m26hP91WalSCS2iDBNYf3LM0l6sYm6e81QhX6Btgq3SuJFXeroExAMvvO6ddPHTt8Mvb9xt5/e6TtZjJcLJfdXKl/oDw4OHbPN/5iZ2lwaAS0VJsSBuQEIST1ZAKpiyBlCZ46QoKlMBpQWnMSq7jNSi8ce37vm/uffaoJEtEdX75nOLdtRz8LUQBI2hYktYzp37Dcmh0vMXd/iwHESoUAlNaKWeslzFGHDyBe+qqaunBu5uz4yTOkWAnREYvQrGbOnPaCxuLoH33zLwueI3MZAdFkoCiM9RKdszlWIa/ZbBYvTE0VWWunsrAQOZOTkxenp6dfHh0dvaPXMjF1tCAx7iVkY9aNJsbms72mGrSSPyfLglm/w61Wc/pn//4ve7/ywAP3xTHAJvK2LYPrGOtAwNnxs/s1QOMHX51+7qknXhNCEglBADET8ytPPj5jlRcEACFdlxzXU9Jx4iSKamBuZvJ5Lg0MFkpDG0b+5qv3/HLHnk8PK60lABKChON4XrZYGBi59rqx/uGR4c07fjU/tnVsWGtklLYShN5gX3RQ2aj5/uSp8QvvnDh6/uiLz5+YPjW+mEThYhS0F7LFMoUkNh565aU3y0ODxetv/dTIbb/1ma/opJtgKHTdqTD72vyUEggC7V+cnppsNxq1mXcmmgf3vXBUKRXNnTsTVCbPK2atHdcRjptxc4VCVgghtNY6jsKwWavVgnarrZJEedmcU+zrz5QHBgsjW64ZGtg0iiOHDzu7br/9xiBBNiNAAQFZacAlO0qfRUfx/SQp1lutcqK0/Nu/e4icycnJ2dOnTx/YtGn01wHI1JQqA56QgbYGWrobuySr+HTdE2utmoXwMs1L/unPv/6dN/b+uPXyD588+8Aj//HlDWPX7E6/nKrcz//76e+++PQTE8d++qwf+n6ktYpZ67Qirnqqyb3XtIrjWMVxaIqfTQDtKAySxuKCm52dGcgWiiPn3zrerzVntErIcT2RL5czxYHBQmlwqC9XKPZ7+WK/l8kUN27brq/ddbOrWSda6agT+JOUjpN1hMi8c+xIY+rUCWbmKGi3F+vzF6frlfnZxkJlvlWrLmilGo7nYebsO31R0B6Mg6CklMqy1s8+euzMV8euve7O1C2l8dX5s2defOvIoROO43rSkVkSwt33g8dbQavVbDXq9WatVq9V5qr1ynwtDoJAa7VUJZfSQb5UchzPE0JK0krrJI6SdqPhx2EQScfRuVJZgNnrG96Qy/cPDnv5wszxV/dHfZs3lzZsHN0SaWRC6uAhKwCPO8CKCMgS0NToV8LJzy/Mi0hIOIVSWbN0k4ihY4aMuBv4Rgz4CvCNRVLGF0gJzE3PTTA4ViA2LoiDMAzffPXl4wdf2Huyo7pEX7j3vs+Bwdzx+lxfqMztf+apg8J1yfM88frzP5tePHu6TiSCoNWa/dYf3zu5/bY9I1EYKoDxyo+envHnZn2rrRCZglxgamlp0VJbbYiop8eoLYzGVjuIAYig1WwErea8qXh7AAQRqFVdoOrstFPsH8x5+UI5DvyCcNxS9JMfemG7JYJWUxl3owEIIR2Ryeelm81qQSIQQvjM3Ih8fzH029UkiVtg9gHESRzJoNWsmAJj2kPs+5Nbtn4bwD/g3aGVQ0SOm8mS47iULeSlm81FUei3g0azyYAftJoNizex4YPWSnGtEjKBUk9oK6FWScJBswk5tkVIz5UkKBcG4WkdNxZ+/L1/bH/yC3ffunXXzRsHB8v9gYJMFITHnXAloI4ARHHwmtDJZP2E8fd//SDT9x77r9/ZvnPn3Tft/sT9ISNjZ1SRAVRqpciA6qlHvvuf+//nB6erlfl6FAaJThKtlFJhEES1izPttJfY03WnHgEnJIRiresAFjogoQzAA4bJaUE1bbG0rRlYrZrEqvij95WU3qbrKoaUVmhi2/ccInKZOQeiPJhzVnjxrhYKiCICQmb2ichn5sCyomqFJnb69kO/6Z8WVujfxUSUcDdmikEUstY2LyJLafQajuNd97xcDl4mC9fLyO237SnliqWtmVzuZimdXcX+gW3X37R75It/eO+tWiNLnchEECAc4yIP7dt3X23+4o9OHjlUdWpxEtUU66ZJ1RV33VvAy9P2c+Pjxx978M9eql6cnZi7cG6qvlBpxGGYaK0YRIo6zPPTV0EM4xwsT66WmpnGlaWuKQLYNSDLGWBpw6ywB0jamviAVQ70NGRXa30BQB2d11RsmuhdwmJWJkHWzKx73iywn9U9nYya6cu5PUlcJxG29mJAgVlZ/FgrAlm7nO77iHwfANSBnzxbG7th+0QmX6hl88UL0nWvnThycPSZRx/+xae+9Pt51pqldF3pSmTyheiNfXsPbxrb8kY+X1C1xUU4M7OzrbEbd4UhQya8POjWVjGvXqtefOGJx38pXPd4HMcn6guVySjwfUMsg1lz1w3FVsYvepIZmwnaNskGOO2VmNojiA+jmA8LfOF79Bvfj3DZsjThKgVxXuV1nv8renny7VNtAJOZXL4qHeecZj2kEz0w/tqrBdbsMGuASBORL6Sc1UrNaKUCAOzUqtXIb/sNZpCwgsW0XJAwEEVR65Gv3/9YFPpH65XK8Xpl7kLotxs9cQyv8jdW0OqVXlyzBUcrvC91NXaO+COw5wcZCoAO/XZiKuoV46YLS1a1Y1AirVTD9I4TAOycf3uiPTfzr8fvuPM3hTAtjpSyNNM78srLT9cW5l+vV+aP1+ZmLzQXF5u49BfwsIaff6/rVyuoPk7DDhNiAzD7Td+lrpUdnjjFYkExA1qpNglZsGtQS30zIarSzZwTrjdbmZpsWK5pfXy8hh3X0nsZCDH+5olmEIXVR7/z7Yeki4ip6wYFATkPOPrzn74Orebq83ProFofvW57RSxIVooa1So1azVndnbu4sCmMbdY7htgk/V86/4//aszbxw+tDAzNTU1Md76EAPo9fERGnTjr+32Zqcmi8Mjm7Z52dyNXjaz9ZN3fX5YgXD0wCvnpiZOTjSqi+ONxYVpk7GpdbatjzWBVSz3UbNe84rlvrLjuhvBPOJ4Xl8URggCvxoH/jQzz5pa0+UE7Ovj4wwsa3VMcS6PToEy/d8tTXSKmOugWh+XDaylYB7v/q9evO7+1sfljv8FCDh08Q/7UaAAAAAASUVORK5CYII=) center center no-repeat";
        snowControl.appendChild(tmpElem);
        tmpElem = tmpElem.cloneNode(true); // snow on dent
        tmpElem.style.width = "26px";
        tmpElem.style.height = "20px";
        tmpElem.style.left = "0";
        tmpElem.style.top = "-9px";
        // Image: controlDentSnow.png
        tmpElem.style.background = "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAUCAYAAACeXl35AAAEGElEQVR4Ae3Sa0ybVRgH8NK3tBS5SClIwbGUC2MzTOwGlm3K0E5gks1lF1D8sgtzw5mwiskkoDHBNF6AZCxOZXG4EeemETKnWwYxzklc2aYlHWIgw02c0A30tdDS9r08/jtOlmZG3Qf5tif55Tk95z3ned7TV/FvcTeIiAMNxEAUhLM5FaMG7v8qpoU0KIatoihakZ+EBZACRljIxtp/OiQMlEEhc0rWaQRoWY4LBAIFkiQ1whB+SxAMEfN7BEEoxXg9bIe1rDG14rZDNaAHAxMLOpkomXWaD2Yo8Pv9zyCPwt8CDYx6vd565Gb8PAhNUAoxof9DLGTBE7iabQKuB6rgOb8oVqP9OqzZRElqxXo3xgLcaXihY2Ji4lbBKFiEgypxFYcwliWZRBEEmeTAbBYwF5CwhnkKgF+azQJI8B/xh91uT0K5sGDBeBQqw923s2KyTyKJF0m6HiD61U/kAreIViXsRB4Lmecx9mAu2IgMt4c825BUWFgYLKhS8Dyvc7vd1cEFEaaw+Tc/ycMzJJ+fIvkbN9G30D+NIj6iX2AIl+TE70sQHF/F3O8CGsLeGTTlAwEkdgteZBQzgkYx7nI9OO3xtGMBbzG7ud9DZJ8i6uYp0DHkGtzS2NT+FU8z/ZgbmCbJMUX+XjfdODnutVub3jnR5ybvgAfFZwANXEZG0zQGk7iBwbGJHhRLu1nwp6HhNZM8/yW6wBWic2z8Dm/0NU+03z54Ln/1U6+mLzbVtP0wcvGY88pol/PKtbbTvf0vtrzbYS4pe2VJ0aqGQ86rF3t4Erv/JDrFo1FAQ3QecJ7vcM/Z11FsHoQrevv6Kkav3ziJtxN/RmedjuEfy3e88JllfUVr7iNFtcnpmeui43SWnOWFO0oqN+/d9Lz18LqtO1uL1m7ck23K25Q4b35p2mJT1ZrddbbaIyc+fOnoqaP7Lly+8N73I84PHCODrWcdp5+urX8WxZKAUxzpOl46fM3VhY9AwP8kvv3JF2fKq3c3my0lW1IzF6yMiddn4sFUjTbSlHB/qiUrd8nqHPMKywN55vxkY3pGuEZjDI/QmpUcVxam5DaqNRGVKRlZ1TnLHq1b+nhxw6L8gu16Q4oZZ8SBUlFvezP3jGOgBR+JYHeTZPv4809rmvdXbdhZY8pdcfPL0jL3QgLoI6NjdMhRoGFriZAFD8HD0br4lQZjenHSfKMFz+ax69RCmCIjO9vQ8v6BihEfiT3j0+MvH/iosaGjc9kbnd3xdQePqYMPMUrgWGZurakgEmJBB3pN5D33QSJ7M6xhLwudSqVaatu7r6p8l3Xbhl3WVZtfeyux7dwl9fFJwmF3GKypEBwAaywkomEhPMapVMvjDcnB11fDnIUaEiADUiGKdTVnEcaKRkIEcDBn8Re/16xasqHKWwAAAABJRU5ErkJggg==) center center no-repeat";
        snowControlDent.appendChild(tmpElem);
        document.body.appendChild(snowControlDent); // Add them together once everything is ready
        document.body.appendChild(snowControl);
    };

    if (document.readyState !== "loading") // init when DOM can be accessed
        init();
    else
        addEvent(document, "DOMContentLoaded", init);
})();
